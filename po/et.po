# libgtop eesti keele tõlge.
# Estonian translation of libgtop.
#
# Copyright (C) 2003, 2005 Free Software Foundation, Inc.
# Copyright (C) 2007 The GNOME Project.
# This file is distributed under the same license as the libgtop package.
#
# Tõivo Leedjärv <toivo@linux.ee>, 2003.
# Ivar Smolin <okul@linux.ee>, 2005, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: libgtop HEAD\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libgtop/issues\n"
"POT-Creation-Date: 2018-01-25 10:19+0000\n"
"PO-Revision-Date: 2019-03-10 19:08+0200\n"
"Last-Translator: Mart Raudsepp <leio@gentoo.org>\n"
"Language-Team: Estonian <gnome-et@linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2\n"

#: lib/read.c:49
#, c-format
msgid "read %d byte"
msgid_plural "read %d bytes"
msgstr[0] "loetud %d bait"
msgstr[1] "loetud %d baiti"

#: lib/read_data.c:49
msgid "read data size"
msgstr "loetud andmete hulk"

#: lib/read_data.c:66
#, c-format
msgid "read %lu byte of data"
msgid_plural "read %lu bytes of data"
msgstr[0] "loetud %lu bait andmeid"
msgstr[1] "loetud %lu baiti andmeid"

#: lib/write.c:49
#, c-format
msgid "wrote %d byte"
msgid_plural "wrote %d bytes"
msgstr[0] "kirjutatud %d bait"
msgstr[1] "kirjutatud %d baiti"

#: src/daemon/gnuserv.c:456
msgid "Enable debugging"
msgstr "Silumise lubamine"

#: src/daemon/gnuserv.c:458
msgid "Enable verbose output"
msgstr "Teaberohke väljundi lubamine"

#: src/daemon/gnuserv.c:460
msgid "Don’t fork into background"
msgstr "Taustale siirdumine keelatud"

#: src/daemon/gnuserv.c:462
msgid "Invoked from inetd"
msgstr "Käivitatakse inetd abil"

#: src/daemon/gnuserv.c:498
#, c-format
msgid "Run “%s --help” to see a full list of available command line options.\n"
msgstr "Kõigi käsureavõtmete nimekirja vaatamiseks käivita „%s --help“.\n"

#: sysdeps/osf1/siglist.c:27 sysdeps/sun4/siglist.c:27
msgid "Hangup"
msgstr "Toru hargilepanek"

#: sysdeps/osf1/siglist.c:28 sysdeps/sun4/siglist.c:28
msgid "Interrupt"
msgstr "Katkestamine"

#: sysdeps/osf1/siglist.c:29 sysdeps/sun4/siglist.c:29
msgid "Quit"
msgstr "Lõpetamine"

#: sysdeps/osf1/siglist.c:30 sysdeps/sun4/siglist.c:30
msgid "Illegal instruction"
msgstr "Lubamatu instruktsioon"

#: sysdeps/osf1/siglist.c:31 sysdeps/sun4/siglist.c:31
msgid "Trace trap"
msgstr "Jälitamispüünis"

#: sysdeps/osf1/siglist.c:32 sysdeps/sun4/siglist.c:32
msgid "Abort"
msgstr "Katkestamine"

#: sysdeps/osf1/siglist.c:33 sysdeps/sun4/siglist.c:33
msgid "EMT error"
msgstr "EMT viga"

#: sysdeps/osf1/siglist.c:34 sysdeps/sun4/siglist.c:34
msgid "Floating-point exception"
msgstr "Ujukomaviga"

#: sysdeps/osf1/siglist.c:35 sysdeps/sun4/siglist.c:35
msgid "Kill"
msgstr "Kõrvaldamine"

#: sysdeps/osf1/siglist.c:36 sysdeps/sun4/siglist.c:36
msgid "Bus error"
msgstr "Siini viga"

#: sysdeps/osf1/siglist.c:37 sysdeps/sun4/siglist.c:37
msgid "Segmentation violation"
msgstr "Segmenteerimisviga"

#: sysdeps/osf1/siglist.c:38 sysdeps/sun4/siglist.c:38
msgid "Bad argument to system call"
msgstr "Vigane süsteemikutsungi argument"

#: sysdeps/osf1/siglist.c:39 sysdeps/sun4/siglist.c:39
msgid "Broken pipe"
msgstr "Katkine toru"

#: sysdeps/osf1/siglist.c:40 sysdeps/sun4/siglist.c:40
msgid "Alarm clock"
msgstr "Alarmkell"

#: sysdeps/osf1/siglist.c:41 sysdeps/sun4/siglist.c:41
msgid "Termination"
msgstr "Lõpetamine"

#: sysdeps/osf1/siglist.c:42 sysdeps/sun4/siglist.c:42
msgid "Urgent condition on socket"
msgstr "Sokli edasilükkamatu seisund"

#: sysdeps/osf1/siglist.c:43 sysdeps/sun4/siglist.c:43
msgid "Stop"
msgstr "Seiskamine"

#: sysdeps/osf1/siglist.c:44 sysdeps/sun4/siglist.c:44
msgid "Keyboard stop"
msgstr "Seiskamine klaviatuurilt"

#: sysdeps/osf1/siglist.c:45 sysdeps/sun4/siglist.c:45
msgid "Continue"
msgstr "Jätkamine"

#: sysdeps/osf1/siglist.c:46 sysdeps/sun4/siglist.c:46
msgid "Child status has changed"
msgstr "Lapsprotsessi olek muudetud"

#: sysdeps/osf1/siglist.c:47 sysdeps/sun4/siglist.c:47
msgid "Background read from tty"
msgstr "Taustalugemine tty'lt"

#: sysdeps/osf1/siglist.c:48 sysdeps/sun4/siglist.c:48
msgid "Background write to tty"
msgstr "Taustakirjutamine tty'le"

#: sysdeps/osf1/siglist.c:49 sysdeps/sun4/siglist.c:49
msgid "I/O now possible"
msgstr "S/V on nüüd võimalik"

#: sysdeps/osf1/siglist.c:50 sysdeps/sun4/siglist.c:50
msgid "CPU limit exceeded"
msgstr "Protsessoriaja piirang ületatud"

#: sysdeps/osf1/siglist.c:51 sysdeps/sun4/siglist.c:51
msgid "File size limit exceeded"
msgstr "Failisuuruse piirang ületatud"

#: sysdeps/osf1/siglist.c:52 sysdeps/sun4/siglist.c:52
msgid "Virtual alarm clock"
msgstr "Virtuaalne alarmkell"

#: sysdeps/osf1/siglist.c:53 sysdeps/sun4/siglist.c:53
msgid "Profiling alarm clock"
msgstr "Alarmkella profileerimine"

#: sysdeps/osf1/siglist.c:54 sysdeps/sun4/siglist.c:54
msgid "Window size change"
msgstr "Akna suuruse muutmine"

#: sysdeps/osf1/siglist.c:55 sysdeps/sun4/siglist.c:55
msgid "Information request"
msgstr "Teabepäring"

#: sysdeps/osf1/siglist.c:56 sysdeps/sun4/siglist.c:56
msgid "User defined signal 1"
msgstr "Kasutaja kirjeldatud signaal 1"

#: sysdeps/osf1/siglist.c:57 sysdeps/sun4/siglist.c:57
msgid "User defined signal 2"
msgstr "Kasutaja kirjeldatud signaal 2"
